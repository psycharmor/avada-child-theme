<?php
/**
 * Tag template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
global $post;
$postid = $post->ID;


?>
<?php get_header(); ?>


<?php


echo "<pre>";
print_r($post);
echo "</pre>";

?>


<section id="content" <?php Avada()->layout->add_class( 'content_class' ); ?> <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php if ( category_description() ) : ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class( 'fusion-archive-description' ); ?>>
			<div class="post-content">
				<?php echo category_description(); ?>
			</div>
		</div>
	<?php endif; ?>

  <?php while ( have_posts() ) : the_post(); ?>
    <?php
    if ($get_post == 'sfwd-courses') {
       get_template_part( 'templates/courses', 'layout' );
    }else {
      get_template_part( 'templates/blog', 'layout' );
    }
    ?>
  <?php endwhile; ?>


</section>
<?php do_action( 'avada_after_content' ); ?>
<?php
get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
