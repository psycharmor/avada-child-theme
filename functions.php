<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' , 9999 );


// enqueue styles for child theme
function learndash_css_enqueue_styles() {

    wp_enqueue_style( 'main-desktop', get_stylesheet_directory_uri().'/css/main-desktop.css' );
    wp_enqueue_style( 'main-global', get_stylesheet_directory_uri().'/css/main-global.css' );
    wp_enqueue_style( 'main-mobile', get_stylesheet_directory_uri().'/css/main-mobile.css' );
    wp_enqueue_style( 'socal-learner', get_stylesheet_directory_uri().'/css/social-learner.css' );
    wp_enqueue_style( 'style-login', get_stylesheet_directory_uri().'/css/style-login.css' );
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );




}
add_action('wp_enqueue_scripts', 'learndash_css_enqueue_styles', 9999 );



include (STYLESHEETPATH . '/includes/category-widget.php');

include (STYLESHEETPATH . '/includes/menu-name.php');

include (STYLESHEETPATH . '/includes/signup-form.php');

include (STYLESHEETPATH . '/includes/video-duration.php');



include (STYLESHEETPATH . '/includes/slider.php');

include (STYLESHEETPATH . '/includes/meta-slide.php');

include (STYLESHEETPATH . '/includes/page-category.php');


include (STYLESHEETPATH . '/includes/pa_courses.php');

include (STYLESHEETPATH . '/includes/recent-post.php');

include (STYLESHEETPATH . '/includes/comment-display.php');



function new_excerpt_more($more) {
    global $post;
    return ' <a class="moretag" href="'. get_permalink($post->ID) . '">[Read More]</a>'; //Change to suit your needs
}

add_filter( 'excerpt_more', 'new_excerpt_more' );



// include (STYLESHEETPATH . '/includes/armor-learndash-functions.php');
// add_filter('template_include','my_custom_search_template');
//
// function my_custom_search_template($template){
//     global $wp_query;
//     if (!$wp_query->is_search)
//         return $template
//
//     return plugin_dir_path( __FILE__ ) . 'armor-learndash/templates/learndash/course_list_template.php';
//
// }




/**
 * Disable free shipping for select products
 *
 * @param bool $is_available
 */
function exclude_shipping_product( $is_available ) {
	global $woocommerce;

	// set the product ids that are ineligible
	$ineligible = array( '14009', '14031' );

	// get cart contents
	$cart_items = $woocommerce->cart->get_cart();

	// loop through the items looking for one in the ineligible array
	foreach ( $cart_items as $key => $item ) {
		if( in_array( $item['product_id'], $ineligible ) ) {
			return false;
		}
	}

	// nothing found return the default value
	return $is_available;
}
add_filter( 'woocommerce_shipping_free_shipping_is_available', 'exclude_shipping_product', 20 );

























/**
 * Proper way to enqueue scripts and styles
 */
function theme_name_scripts()
{
  wp_enqueue_script( 'header', get_stylesheet_directory_uri() . '/js/header.js', array( 'jquery' ) );

}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );



function my_custom_get_posts( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( $query->is_archive() ) {

        $query->set( 'category__not_in', array( 7459 ) );
    }
}
add_action( 'pre_get_posts', 'my_custom_get_posts', 1 );
