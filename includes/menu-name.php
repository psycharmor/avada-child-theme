<?php

function give_profile_name(){
    $user=wp_get_current_user();
    if(!is_user_logged_in())
        $name = "User not logged in";
    else
         $name=$user->user_firstname.' '.$user->user_lastname;
    return $name;
}
add_shortcode('profile_name', 'give_profile_name');



function my_dynamic_menu_items( $menu_items ) {
    foreach ( $menu_items as $menu_item ) {
        if ( strpos($menu_item->title, '#profile_name#') !== false) {
                $menu_item->title =  str_replace("#profile_name#",  wp_get_current_user()->user_login, $menu_item->title);
        }
        if ( strpos($menu_item->title, '#bookmark#') !== false) {
                $menu_item->title =  str_replace("#bookmark#",  '<i class="fas fa-bookmark"></i>', $menu_item->title);
        }
        if ( strpos($menu_item->title, '#setting#') !== false) {
                $menu_item->title =  str_replace("#setting#",  '<i class="fas fa-cog"></i>', $menu_item->title);
        }
    }
    return $menu_items;
}
add_filter( 'wp_nav_menu_objects', 'my_dynamic_menu_items' );
