<?php


// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget
class wpb_widget extends WP_Widget {

  function __construct() {
    parent::__construct(

    // Base ID of your widget
    'wpb_widget',

    // Widget name will appear in UI
    __('(PsychArmor) - Sponsored by', 'wpb_widget_domain'),

    // Widget description
    array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), )
    );
  }

  // Creating widget front-end

  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
      // echo $args['before_title'] . $title . $args['after_title'];
      echo __( 'Sponsored by', 'wpb_widget_domain' );
      ?>
      <ul>
        <?php

        $category_list = wp_get_post_terms( $post->ID, array( '' ) );
        // $ld_category_list = wp_get_post_terms( $post->ID, array( 'ld_course_category',  'ld_course_tag' ) );
        $ld_category_list = get_the_terms( $post->ID, array( 'ld_course_category' ) );

        $ld_tag_list = get_the_terms(get_the_ID(), 'ld_course_tag' );

         ?>
        <?php foreach ($ld_tag_list as $key => $cat) : ?>
        <li>
          <img src="<?php echo z_taxonomy_image_url($cat->term_id , array(200, 200)); ?>" />
        </li>
        <?php endforeach; ?>
      </ul><?php
    // This is where you run the code and display the output

    echo $args['after_widget'];
  }

  // Widget Backend
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    }
    else {
      $title = __( 'New title', 'wpb_widget_domain' );
    }
    // Widget admin form
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
  }
}
