<?php

function learnarmor_child_custom_excerpts($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}

// add_filter( 'excerpt_length', 'learnarmor_custom_excerpt_length', 999 );

// add_post_type_support('sfwd-courses', 'excerpt');

function pa_recent_post_shortcode($atts = [], $content = null, $tag = '') {
    // override default attributes with user attributes
    $a = shortcode_atts( array(
            "post_type"         => '',
            "taxonomy"          =>'',
            "term"              => '',
            "class"             => '',
            "posts_per_page"    => '',
            "order"             => '',
            "orderby"          => '',
            "excerpt_length"    => '',
    ), $atts, $tag);
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    $query = new WP_Query(
        array(
            'post_status'       => 'publish',
            "post_type"         => $a['post_type'],// not "post-type" !
            "taxonomy"          => $a['taxonomy'],
            "term"              => $a['term'],
            "class"             => $a['class'],
            "posts_per_page"    => $a['posts_per_page'],
            "orderby"           => $a['orderby'],
            "order"             => $a['order']
            //'caller_get_posts'  => 1
        ) );
    $content = '';


    ?>

  <style>
  .recent-post-wrap-text {
    background: #46535E;
    color: #F2F5FA;
    padding: 10px;
    margin-top: -10px;
  }
  p.recent-post-title {
    font-size: 21px !important;
}
  </style>


    <?php
    while ($query->have_posts()) : $query->the_post();

    /* grab the url for the full size featured image */
    $featured_img_url = get_the_post_thumbnail_url($query->ID, 'full');
    $the_title = get_the_title();
    $the_content = get_the_excerpt();
    $post_url = get_post_permalink();
    $content .='<div class="' . $a['class'] . '">';
    $content .= '<a href="' . $post_url . '" >';
    $content .= '<img src="' . $featured_img_url . '" alt="' . $the_title . ' ' . 'featured content' . '"' . '/>';
    $content .= '<div class="recent-post-wrap-text">';
    $content .= '<p class="recent-post-title">'. $the_title .'</p>';
    $content .= '<p class="recent-post-excerpt">'. $the_content .'</p>';


    // $content .='<video class="home-slider-content-media" autoplay loop muted> <source src="https://vimeo.com/326010853" type="video/webm"> </video>';
    // $content .= '<iframe src="https://player.vimeo.com/video/12345?autoplay=1&loop=1&autopause=0" onclick="openModal(this) width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

    // $content .= '<a class="preview-course" target="_blank" href="https://player.vimeo.com/video/294477856" onclick="openModal(this)"><i class="fa fa-play"> </i> Preview Course</a>';
    // $content .= '<a class="preview-course" target="_blank" href="http://player.vimeo.com/external/85569724.sd.mp4?s=43df5df0d733011263687d20a47557e4" onclick="openModal(this)"><i class="fa fa-play"> </i> Preview Course</a>';
    $content .='</div></a></div>';

       // return $content;
    endwhile;



return html_entity_decode($content);
   wp_reset_query();
}
add_shortcode("recent_post", "pa_recent_post_shortcode");
