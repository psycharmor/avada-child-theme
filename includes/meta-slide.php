<?php




/**
 * Register meta boxes.
 */
function pa_register_meta_boxes() {
    add_meta_box( 'hcf-1', __( 'Homepage Slider', 'hcf' ), 'pa_display_callback', 'page' );
}
add_action( 'add_meta_boxes', 'pa_register_meta_boxes' );

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function pa_display_callback( $post ) {

  ?>
  <div class="pa_box">
      <style scoped>
          .pa_box{
              display: grid;
              grid-template-columns: max-content 1fr;
              grid-row-gap: 10px;
              grid-column-gap: 20px;
          }
          .pa_field{
              display: contents;
          }
      </style>
      <p class="meta-options pa_field">
          <label for="pa_tab_name">Tab Name</label>
          <input id="pa_tab_name"
              type="text"
              maxlength="160"
              name="pa_tab_name"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_tab_name', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_video">Background Video</label>
          <input id="pa_video"
              type="url"
              name="pa_video"
              placeholder="Example ( youtube.com/12333)"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_video', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_desc_title">Description Title</label>
          <input id="pa_desc_title"
              type="text"
              maxlength="160"
              name="pa_desc_title"
              placeholder="Example ( Description Title )"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_desc_title', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_description">Short description</label>
          <input id="pa_description"
              type="text"
              maxlength="250"
              name="pa_description"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_description', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_btn1_label">1 Button Name</label>
          <input id="pa_btn1_label"
              type="text"
              name="pa_btn1_label"
              placeholder="Example ( Label )"
             value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_btn1_label', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_btn1_link">1 Button Url</label>
          <input id="pa_btn1_link"
              type="url"
              name="pa_btn1_link"
              placeholder="Example ( /courses-for-financial-wellness )"
             value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_btn1_link', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_btn2_label">2 Button Name</label>
          <input id="pa_btn2_label"
              type="text"
              name="pa_btn2_label"
              placeholder="Example ( Label )"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_btn2_label', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_btn2_link">2 Button Url</label>
          <input id="pa_btn2_link"
              type="url"
              name="pa_btn2_link"
              placeholder="Example ( /what-we-do )"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_btn2_link', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_btn3_label">3 Button Name</label>
          <input id="pa_btn3_label"
              type="text"
              name="pa_btn3_label"
              placeholder="Example ( Label )"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_btn3_label', true ) ); ?>">
      </p>
      <p class="meta-options pa_field">
          <label for="pa_btn3_link">3 Button Url</label>
          <input id="pa_btn3_link"
              type="url"
              name="pa_btn3_link"
              placeholder="Example ( /courses)"
              value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'pa_btn3_link', true ) ); ?>">
      </p>
  </div>

  <?php

}

// 1920 × 433

/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function pa_save_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }
    $fields = [
        'pa_tab_name',
        'pa_video',
        'pa_desc_title',
        'pa_description',
        'pa_btn1_label',
        'pa_btn1_link',
        'pa_btn2_label',
        'pa_btn2_link',
        'pa_btn3_label',
        'pa_btn3_link',

    ];
    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, sanitize_text_field( $_POST[$field] ) );
        }
     }
}
add_action( 'save_post', 'pa_save_meta_box' );
