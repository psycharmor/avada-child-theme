<?php
// Updated version for course layout shortcode

function pa_query_shortcode($atts = [], $content = null, $tag = '') {
    // override default attributes with user attributes
    $a = shortcode_atts( array(
            "post_type"         => '',
            "taxonomy"          =>'',
            "term"              => '',
            "class"             => '',
            "posts_per_page"    => '',
            "order"             => '',
            "orderby"          => '',
            "excerpt_length"    => '',
    ), $atts, $tag);
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    $query = new WP_Query(
        array(
            'post_status'       => 'publish',
            "post_type"         => $a['post_type'],// not "post-type" !
            "taxonomy"          => $a['taxonomy'],
            "term"              => $a['term'],
            "class"             => $a['class'],
            "posts_per_page"    => $a['posts_per_page'],
            "orderby"           => $a['orderby'],
            "order"             => $a['order']
            //'caller_get_posts'  => 1
        ) );
    $content = '';
    while ($query->have_posts()) : $query->the_post();

    /* grab the url for the full size featured image */
    $featured_img_url = get_the_post_thumbnail_url($query->ID, 'full');

    $author_id = get_post_field ('post_author', $query->ID);

    $display_name = get_the_author_meta( 'display_name' , $author_id );

    $meta_name = get_post_meta( get_the_ID(),'_sfwd-courses',true );

    $course_video_embed = get_post_meta( get_the_ID(), '_boss_edu_post_video', true );

    // if ( 'http' == substr( $course_video_embed, 0, 4 ) ) {
    //   // V2 - make width and height a setting for video embed
    //   $course_video_embed = wp_oembed_get( esc_url( $course_video_embed )/* , array( 'width' => 100 , 'height' => 100) */ );
    // }
    //
    // if ( '' != $course_video_embed ) {
    //     echo '<div class="course-video">'.html_entity_decode( $course_video_embed.'</div>';
    //   }

    // get_the_author_meta( 'sfwd-courses',$query->ID ) );
    // echo "<pre>";
    // print_r($author_id);
    // echo "</pre>";


// <div id="course-video">
//   <a href="#" id="hide-video" class="button"><i class="fa fa-close"></i></a>
// </div>

?>


<style media="screen">

p.course-title {
    font-size: 18px;
}

p.subject-matter-expert {
    font-size: 10px;
}

.wrap-text {
    background: #4168B0;
    margin-top: -10px;
    padding: 20px;
}
.wrap-text a {
    color: white;
}
.featured-courses {
    margin-bottom: 15px;
}

  /* .wrap-text{
    position: absolute;
    background: red;
    padding: 10px;
    top: 76px;
    width: 328px;
  } */
  i.fa.fa-play{
    background: #c3c3c3;
    padding: 5px 25px 5px 20px;
    width: 35px;
    border-radius: 3px;
    color: #4167b0;
  }
</style>

<script type="text/javascript">

function openModal(obj, href){
    window.open(obj.href, 'windowName', 'width=700, height=550, left=150, top=50, scrollbars, resizable, allow="autoplay');
    return false;
}

</script>

<?php



    $the_title = get_the_title();
    $post_url = get_post_permalink();
        $content .='<div class="' . $a['class'] . '">';
        $content .= '<a href="' . $post_url . '" >';
        $content .= '<img src="' . $featured_img_url . '" alt="' . $the_title . ' ' . 'featured content' . '"' . '/>';
        $content .= '<div class="wrap-text">';
        $content .= '<p class="course-title">'. $the_title .'</p>';
        $content .='<p class="subject-matter-expert"> Subject Matter Expert: '. $display_name .'</p>';


        // $content .='<video class="home-slider-content-media" autoplay loop muted> <source src="https://vimeo.com/326010853" type="video/webm"> </video>';
        // $content .= '<iframe src="https://player.vimeo.com/video/12345?autoplay=1&loop=1&autopause=0" onclick="openModal(this) width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

        $content .= '<a class="preview-course" target="_blank" href="https://player.vimeo.com/video/294477856" onclick="openModal(this)"><i class="fa fa-play"> </i> Preview Course</a>';
        // $content .= '<a class="preview-course" target="_blank" href="http://player.vimeo.com/external/85569724.sd.mp4?s=43df5df0d733011263687d20a47557e4" onclick="openModal(this)"><i class="fa fa-play"> </i> Preview Course</a>';
        $content .='</div></a></div>';

        ?>

        <?php

       // return $content;
    endwhile;

return html_entity_decode($content);
   wp_reset_query();
}
add_shortcode("featured_content", "pa_query_shortcode");
