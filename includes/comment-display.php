<?php

// This contains the function that handles the comment slider in the bottom of the homepage
// the function is wrapped around a shortcode
// EXAMPLE: [display_comments status="approve" number="3" orderby="comment_date"]

function print_comment($comment, $max_comment_length) {
    // cuts of comment if the comment is greater than max length
    // otherwise return comment unchanged
    if (strlen($comment) > $max_comment_length)
        return substr($comment, 0, $max_comment_length - 3) . "...";

    return $comment;
}

function pa_comment_shortcode($atts = [], $content = null, $tag = '') {

    // override default attributes with user attributes
    $args = shortcode_atts( array(
        "class"   => '',
        "status"  => '', // hold, approve, all
        "number"  => '',
        "orderby" => '',
        "order"   => ''  // asc/desc; desc -> latest
    ), $atts, $tag);

    // normalize attribute keys, lowercase
    $args = array_change_key_case($args, CASE_LOWER);
    $content = '';

    $comments = get_comments($args);
    $max_comment_length = 100;
    ?>

    <style>

        .home-page-comment-slider {
            background-image: url("wp-content/themes/Avada-Child-Theme/image/quote.png");
            background-size: 100%;
            background-repeat: no-repeat;
            margin: 0 auto;
            height: 300px;
            max-width: 730px;
        }

        .home-page-comment-content {
            position: relative;
            top: 10px;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 0 auto;
            height: 200px;
            max-width: 550px;
            padding-top: 13px;
            padding-bottom: 35px;
        }

        .home-page-comment {
            position: absolute;
            text-align: center;
            color: #4168B1;
            font-size: 24px;
        }

        @media screen and (max-width: 750px)
        {
            .home-page-comment-slider {
                background-image: none;
            }

            .home-page-comment-content {
                border: 8px solid #52C5D8;
                border-radius: 20px;
                top: 0;
                margin: 0 auto;
                padding: 0;
                width: 90%;
            }

            .home-page-comment {
                font-size: 18px;
                padding: 15px;
            }
        }

    </style>

    <script type="text/javascript">
        // handles the actual slideshow
        $j = jQuery.noConflict();
        $j(document).ready(function() {
            // set all comments except first to hide
            $j('.home-page-comment:gt(0)').hide();

            // slideshow transition
            //  1. fadeOut()        -> hide first comment in list
            //  2. next(), fadeIn() -> get the next element (2nd in list) and show it
            //  3. end()            -> go back to the first element
            //  4. appendTo()       -> append the first element to the end of the list (2nd in list is now first)
            setInterval(function() {
                $j('.home-page-comment:first')
                    .fadeOut(500)
                    .next()
                    .fadeIn(500)
                    .end()
                    .appendTo(".home-page-comment-content");
            }, 5000);
        });

    </script>

    <?php
        if ($args['class'])
            $content .= "<div class='" . $args['class'] . "'>";
        else
            $content .= "<div class='home-page-comment-slider'>";
        $content .= "<div class='home-page-comment-content'>";
        foreach ($comments as $comment) {
            $content .= "<div class='home-page-comment'>";
            $content .= "\"" . print_comment($comment->comment_content, $max_comment_length) . "\"<br>";
            $content .= "-- " . $comment->comment_author;
            $content .= "</div>";
        }
        $content .= "</div>";
        $content .= "</div>";
    ?>

    <?php
    return html_entity_decode($content);
    wp_reset_query();
}
add_shortcode("display_comments", "pa_comment_shortcode");
?>
