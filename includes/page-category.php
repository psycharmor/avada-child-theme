<?php

// add tag and category support to pages
function tags_categories_support_all() {
  register_taxonomy_for_object_type('post_tag', 'page');
  register_taxonomy_for_object_type('category', 'page');
}

// ensure all tags and categories are included in queries
function tags_categories_support_query($wp_query) {
  if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
  if ($wp_query->get('category_name')) $wp_query->set('post_type', 'any');
}

// tag and category hooks
add_action('init', 'tags_categories_support_all');
add_action('pre_get_posts', 'tags_categories_support_query');




function addTitleFieldToCat(){
    $cat_title = get_term_meta($_POST['tag_ID'], '_pagetitle', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="cat_page_title"><?php _e('Category Page Title'); ?></label></th>
        <td>
        <input type="text" name="cat_title" id="cat_title" value="<?php echo $cat_title ?>"><br />
            <span class="description"><?php _e('Title for the Category '); ?></span>
        </td>
    </tr>
    <?php

}
add_action ( 'edit_category_form_fields', 'addTitleFieldToCat');

function saveCategoryFields() {
    if ( isset( $_POST['cat_title'] ) ) {
        update_term_meta($_POST['tag_ID'], '_pagetitle', $_POST['cat_title']);
    }
}
add_action ( 'edited_category', 'saveCategoryFields');
