<?php

// [slider_content post_type="page" taxonomy="category" term="slider" posts_per_page="8" class="" orderby="title" order="ASC"]


function custom_query_shortcode($atts = [], $content = null, $tag = '') {
    // override default attributes with user attributes
    $a = shortcode_atts( array(
            "post_type"         => '',
            "taxonomy"          =>'',
            "term"              => '',
            "class"             => '',
            "posts_per_page"    => '',
            "order"             => '',
            "orderby"          => '',
            "excerpt_length"    => '',
    ), $atts, $tag);
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    $query = new WP_Query(
        array(
            'post_status'       => 'publish',
            "post_type"         => $a['post_type'],// not "post-type" !
            "taxonomy"          => $a['taxonomy'],
            "term"              => $a['term'],
            "class"             => $a['class'],
            "posts_per_page"    => $a['posts_per_page'],
            "orderby"           => $a['orderby'],
            "order"             => $a['order']
            //'caller_get_posts'  => 1
        ) );
    $content = '';



    while ($query->have_posts()) : $query->the_post();

    /* grab the url for the full size featured image */
    $featured_img_url = get_the_post_thumbnail_url($query->ID, array( 1920 , 100) );


    $featured_image_2 = get_post_meta( get_the_ID(),'kd_featured-image-2_page_id',true );
    $featured_image_3 = get_post_meta( get_the_ID(),'kd_featured-image-3_page_id',true );
    $featured_image_4 = get_post_meta( get_the_ID(),'kd_featured-image-4_page_id',true );
    $featured_image_5 = get_post_meta( get_the_ID(),'kd_featured-image-5_page_id',true );
    $featured_image_6 = get_post_meta( get_the_ID(),'kd_featured-image-6_page_id',true );
    $featured_image_7 = get_post_meta( get_the_ID(),'kd_featured-image-7_page_id',true );
    $featured_image_8 = get_post_meta( get_the_ID(),'kd_featured-image-8_page_id',true );
    $featured_image_9 = get_post_meta( get_the_ID(),'kd_featured-image-9_page_id',true );
    $featured_image_10 = get_post_meta( get_the_ID(),'kd_featured-image-10_page_id',true );

    $meta_name = get_post_meta( get_the_ID(),'pa_tab_name',true );
    $meta_video = get_post_meta( get_the_ID(),'pa_video',true );
    $meta_title = get_post_meta( get_the_ID(),'pa_desc_title',true );
    $meta_description = get_post_meta( get_the_ID(),'pa_description',true );
    $meta_pa_btn1_label = get_post_meta( get_the_ID(),'pa_btn1_label',true );
    $meta_pa_btn1_link = get_post_meta( get_the_ID(),'pa_btn1_link',true );
    $meta_pa_btn2_label = get_post_meta( get_the_ID(),'pa_btn2_label',true );
    $meta_pa_btn2_link = get_post_meta( get_the_ID(),'pa_btn2_link',true );
    $meta_pa_btn3_label = get_post_meta( get_the_ID(),'pa_btn3_label',true );
    $meta_pa_btn3_link = get_post_meta( get_the_ID(),'pa_btn3_link',true );

    $the_title = get_the_title();
    $post_url = get_post_permalink();



    $myImages[] = array(
               $featured_image_2,
               $featured_image_3,
              $featured_image_4,
              $featured_image_5,
              $featured_image_6,
               $featured_image_7,
               $featured_image_8,
               $featured_image_9,
               $featured_image_10)
              ;


   // $attachment_data = Avada()->images->get_attachment_data( $mySlider );
   //
   //  foreach ($myImages as $key => $images ) {
   //
   //    foreach ($images as $key => $image) {
   //
   //      $attachment_data = Avada()->images->get_attachment_data( $image );
   //
   //      echo "<pre>";
   //      print_r($attachment_data);
   //      echo "</pre>";
   //    }
   //
   //  }

  $myArray[] = array(
                  'pa_tab_name' => $meta_name,
                  'pa_video' => $meta_video,
                  'pa_img' => $featured_img_url,
                  'pa_desc_title' => $meta_title,
                  'pa_description' => $meta_description,
                  'pa_btn1_label' => $meta_pa_btn1_label,
                  'pa_btn1_link'  => $meta_pa_btn1_link,
                  'pa_btn2_label' => $meta_pa_btn2_label,
                  'pa_btn2_link'  => $meta_pa_btn2_link,
                  'pa_btn3_label' => $meta_pa_btn3_label,
                  'pa_btn3_link'  => $meta_pa_btn3_link,)
                  ;


endwhile;
  ?>

<style>


/*--------------------------------------------------------------
This is your custom stylesheet.

Add your own styles here to make theme updates easier.
To override any styles from other stylesheets, simply copy them into here and edit away.

Make sure to respect the media queries! Otherwise you may
accidentally add desktop styles to the mobile layout.

You're the boss, so have fun editing!

--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 - Global Styles
2.0 - Responsive media queries (mobile styles)
	2.1 - MAXIMUM width of 480 pixels (phones and smaller)
	2.2 - MINIMUM width of 481 pixels (phones and larger)
	2.3 - MINIMUM width of 721 pixels (tablets and larger)
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 - Global Styles
--------------------------------------------------------------*/

  </style>
  <!-- START OF YOUR CODE -->
     <?php $first_item = $myArray[0]['pa_tab_name']; ?>
      <ul class="home-slider-bar nav nav-tabs"> <?php
        $current_tab = $myArray[0]['pa_tab_name'];
        foreach ($myArray as $row):
            $tab_class = ($row['pa_tab_name']==$current_tab) ? 'active' : '' ;
            echo '<li class="nav-item home-slider-tab '.$tab_class.'">
                      <a class="home-slider-link nav-link" href="#' . str_replace(' ', '', $row['pa_tab_name'] )  .  '" data-toggle="tab"><p class="aligncenter" >' . $row['pa_tab_name'] .  '</p> </a>
                 </li>';
        endforeach;
        ?>
     </ul>
     <div class="tab-content">
         <?php foreach ($myArray as $array) { ?>
             <div class="<?php echo "tab-pane ", ($array['pa_tab_name'] === $first_item ? "home-slider-content active" : "home-slider-content"); ?>"
                 id="<?php echo str_replace(' ', '', $array['pa_tab_name']); ?>" aria-labelledby="<?php echo str_replace(' ', '', $array['pa_tab_name']) . "-tab"; ?>">

                 <?php if ( ! empty( $array['pa_video'] ) ) {
                   echo '<video class="home-slider-content-media" autoplay loop muted>
                       <source src="'.$array['pa_video'].'" type="video/mp4">
                   </video>';
                 }else{
                   echo '<img class="home-slider-content-media" src="'.$array['pa_img'].'">';
                 }
                 ?>
                 <div class="row home-slider-content-footer">
                     <div class="col-xs-12 col-sm-6">
                         <div class="home-slider-content-footer-desc">
                             <p><span style="font-weight: bold;"><?php echo $array['pa_desc_title']; ?></span></p>
                             <span><?php echo $array['pa_description']; ?></span>
                         </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 home-slider-content-footer-btns">
                       <?php if ( ! empty( $array['pa_btn1_link'] && $array['pa_btn1_label'] ) ) {
                                echo '<a class="btn home-slider-content-footer-btn" href="'.$array['pa_btn1_link'].'" target="_blank">'.$array['pa_btn1_label'].'</a>' ;
                            }
                            if ( ! empty( $array['pa_btn2_link'] && $array['pa_btn2_label'] ) ) {
                                echo '<a class="btn home-slider-content-footer-btn" href="'.$array['pa_btn2_link'].'" target="_blank">'.$array['pa_btn2_label'].'</a>' ;
                            }
                            if ( ! empty( $array['pa_btn3_link'] && $array['pa_btn3_label'] ) ) {
                                echo '<a class="btn home-slider-content-footer-btn" href="'.$array['pa_btn3_link'].'" target="_blank">'.$array['pa_btn3_label'].'</a>';
                            }
                         ?>
                     </div>
                 </div>
             </div>
         <?php } ?>
     </div>
  <?php
return html_entity_decode($content);
   wp_reset_query();
}
add_shortcode("slider_content", "custom_query_shortcode");

?>
