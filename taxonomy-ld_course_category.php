<?php
/**
 * Archives template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<?php  $get_post = get_post_type( get_the_ID() ); ?>

<?php

//
// $cat_args = array(
//     'taxonomy' => 'faq_type',
//     'exclude'  => array(7),
//     'orderby'  => 'simple_page_ordering_is_sortable'
// );
//
// $categories = get_terms( $cat_args );
//
// foreach ( $categories as $category )
// {
//     $args = array(
//         'post_type'      => 'faq',
//         'posts_per_page' => -1, // load all posts
//         'orderby'        => 'simple_page_ordering_is_sortable',
//         'tax_query'      => array(
//             array(
//                 'taxonomy' => 'faq_type',
//                 'field'    => 'slug',
//                 'terms'    => $category->slug
//             )
//         )
//     );
//
//     $cat_query = new WP_Query( $args );
//
//     // enter the rest of your code below
// }
//
//
//
// $cat_args = array(
//     'taxonomy' => 'ld_course_category',
//     'orderby'  => 'simple_page_ordering_is_sortable'
// );
//
// $categories = get_terms( $cat_args );
//
//
// echo "<pre>";
// print_r($categories);
// echo "</pre>";
//
// foreach ( $categories as $category )
// {
//     $posts = get_posts(array(
//         'numberposts'   => -1, // get all posts.
//         'tax_query' => array(
//             array(
//                 'taxonomy' => 'ld_course_category',
//                 'field'    => 'slug',
//                 'terms'     => $category->slug,
//                 'operator' => 'IN',
//             ),
//         ),
//         'post_type'     => 'faq',
//     ));
//
//
//
// }
//
//
// die;
//
// $args = array(
//     'post_type' => $get_post,
//     'category_name' => 'category-name'
// );
// $the_query = new WP_Query( $args );

// foreach ($post as $key => $value) {
//

// }
//
// die;
?>

<section id="content" <?php Avada()->layout->add_class( 'content_class' ); ?> <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php if ( category_description() ) : ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class( 'fusion-archive-description' ); ?>>
			<div class="post-content">
				<?php
				if ($get_post == 'sfwd-courses') {

				}else {
					echo category_description();
				}
				?>
			</div>
		</div>
	<?php endif; ?>

  <?php while ( have_posts() ) : the_post(); ?>
    <?php
    if ($get_post == 'sfwd-courses') {
       get_template_part( 'templates/courses', 'layout' );
    }else {
      get_template_part( 'templates/blog', 'layout' );
    }
    ?>
  <?php endwhile; ?>


</section>
<?php do_action( 'avada_after_content' ); ?>
<?php
get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
