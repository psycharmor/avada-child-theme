<div class="stm_lms_search_popup">
    <div class="stm_lms_search_popup__close">
        <i class="fas fa-times"></i>
    </div>
    <div class="inner">
        <h2><?php esc_html_e('Search', 'Avada'); ?></h2>
        <div class="header_top">
              <div class="stm_courses_search">
                <?php get_search_form();?>
          		    <?php get_template_part( 'templates/categories', 'categories' ); ?>

              </div>
        </div>
    </div>
</div>
