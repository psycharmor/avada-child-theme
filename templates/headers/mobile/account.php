<?php
// stm_module_styles('header_mobile', 'account');
// if (class_exists('STM_LMS_User')):
// 	$messages = 0;
// 	if (is_user_logged_in()) {
// 		$target = 'stm-lms-modal-become-instructor';
// 		$modal = 'become_instructor';
// 	} else {
// 		$target = 'stm-lms-modal-login';
// 		$modal = 'login';
// 	}

	?>

    <div class="stm_lms_account_popup">
        <div class="stm_lms_account_popup__close">
						<i class="fas fa-times"></i>
        </div>
        <div class="inner">
			<?php if (is_user_logged_in()):
					$user = wp_get_current_user();
					// if ( ($user instanceof WP_User) ) {
			    //      echo 'Welcome : ' . esc_html( $current_user->display_name );
			    //      echo get_avatar( $user->ID, 32 );
			    //  }
					?>
                <div class="stm_lms_account_popup__user">
									<?php echo get_avatar( $user->ID, 32 ); ?>
                    <div class="stm_lms_account_popup__user_info">
                        <h4><?php echo sanitize_text_field( $user->display_name ); ?></h4>
												<a href='<?php echo get_edit_user_link(); ?>'><?php esc_html_e( 'Edit profile', 'Avada' ); ?></a>
                    </div>
                </div>

            <div class="stm_lms_account_popup__list heading_font">
                <a class="stm_lms_account_popup__list_single"
                   href="/courses">
					<?php esc_html_e('My Courses', 'Avada'); ?>
                </a>

								<a class="stm_lms_account_popup__list_single"
									 href="/my-transcripts/">
					<?php esc_html_e('My Transcripts', 'Avada'); ?>
								</a>

								<?php if( class_exists('Woocommerce')){ ?>
									<a class="stm_lms_account_popup__list_single"
										 href="/checkout">
						<?php esc_html_e('Checkout', 'Avada'); ?>
									</a>
								<?php } ?>

                <a class="stm_lms_account_popup__list_single has_number"
                   href="/bookmark">
					<?php esc_html_e('Favorites', 'Avada'); ?>
                </a>

								<a class="stm_lms_account_popup__list_single"
                   href="/edit-profile">
					<?php esc_html_e('Settings', 'Avada'); ?>
                </a>
								<a class="stm_lms_account_popup__list_single"
									 href="/donate">
					<?php esc_html_e('Donate', 'Avada'); ?>
								</a>
								<a class="stm_lms_account_popup__list_single"
									 href="/partnership">
					<?php esc_html_e('Become an Partner', 'Avada'); ?>
								</a>
                <a class="stm_lms_account_popup__list_single"
                   href="<?php echo wp_logout_url('/'); ?>">
					<?php esc_html_e('Logout', 'Avada'); ?>
                </a>
            </div>
					<?php else: ?>
						 <div class="stm_lms_account_popup__list_logout">
							<a href="<?php echo wp_login_url( get_permalink() ); ?>" class="stm_lms_account_popup__login" title="Login">
										<h3><?php esc_html_e('Login/Sign Up', 'Avada'); ?></h3>
							</a>

							<a class="stm_lms_account_popup__list_single"
								 href="<?php echo wp_login_url( get_permalink() ); ?>">
				<?php esc_html_e('Login', 'Avada'); ?>
							</a>

							<a class="stm_lms_account_popup__list_single"
								 href="<?php echo wp_login_url( get_permalink() ); ?>">
				<?php esc_html_e('Signup', 'Avada'); ?>
							</a>
							<a class="stm_lms_account_popup__list_single"
								 href="/donate">
				<?php esc_html_e('Donate', 'Avada'); ?>
							</a>
							<a class="stm_lms_account_popup__list_single"
								 href="/partnership">
				<?php esc_html_e('Become an Partner', 'Avada'); ?>
							</a>


					<?php endif; ?>

        </div>
    </div>
