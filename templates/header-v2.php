<?php
/**
 * Header-v2 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
			<div class="fusion-header-has-flyout-menu-content">
		<?php endif; ?>


		<div class="logo-unit">
			<?php avada_logo(); ?>
		</div>
		<div class="center-unit col-md-6">
			<div class="stm_courses_search">
				<div class="ld_categories">
						<?php get_template_part( 'templates/categories', 'categories' ); ?>
				</div>
				<div class="stm_lms_courses_search">
					<?php get_search_form();?>
				</div>
			</div>

		</div>
		<?php avada_main_menu(); ?>
		<!-- <div class="right-unit">

		</div> -->


		<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
			</div>
		<?php endif; ?>
	</div>
</div>
