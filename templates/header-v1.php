<?php
/**
 * Header-v1 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
			<div class="fusion-header-has-flyout-menu-content">
		<?php endif; ?>


		<div class="logo-unit">
			<?php avada_logo(); ?>
		</div>
		<div class="center-unit">
			<div class="stm_courses_search">
				<div class="ld_categories">
						<?php get_template_part( 'templates/categories', 'categories' ); ?>
				</div>
				<div class="stm_lms_courses_search">
					<?php get_search_form();?>
				</div>
			</div>

			<div class="stm_header_links">
				<a href="#" class="stm_lms_bi_link normal_font" data-target=".stm-lms-modal-become-instructor" data-lms-modal="become_instructor">
					<i class="lnr lnr-bullhorn secondary_color"></i>
			        <span>Become an Instructor</span>
			    </a>
			<a href="#" class="stm_lms_bi_link normal_font" data-target=".stm-lms-modal-enterprise" data-lms-modal="enterprise">
			    <i class="stmlms-case secondary_color"></i>
			    	<span>For Enterprise</span>
			</a>
		</div>

		</div>
		<div class="right-unit">
			<?php avada_main_menu(); ?>
		</div>

		<!-- </div> -->
		<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
			</div>
		<?php endif; ?>
	</div>
</div>
