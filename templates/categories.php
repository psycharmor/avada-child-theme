<?php
// stm_module_styles('lms_categories_megamenu', 'style_1');

$parent_terms = get_terms( array(
	'taxonomy' => 'ld_course_category',
	'hide_empty' => false,
	'parent' => 0,
) );


// echo "<pre>";
// print_r($parent_terms);
// echo "</pre>";


if(!empty($parent_terms) and !is_wp_error($parent_terms)): ?>
<div class="ld_categories">
  <!-- <i class="ld-hamburger"></i> -->
	<i class="fas fa-bars"></i>
	<span class="heading_font"><?php esc_html_e('Course Category', 'avada'); ?></span>

    <div class="ld_categories_dropdown">
        <div class="ld_categories_dropdown__parents">
            <?php foreach($parent_terms as $term):
                $parent_id = $term->term_id;

                $child_terms = get_terms( array(
        					'taxonomy' => 'ld_course_category',
        					'hide_empty' => false,
        					'parent' => $parent_id,
        				) );

								// echo "<pre>";
								// print_r($term->description);
								// echo "</pre>";
                  ?>
                <div class="ld_categories_dropdown__parent">
                    <a href="<?php echo $term->description; ?>" class="sbc_h">
                        <?php echo sanitize_text_field($term->name); ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif;
